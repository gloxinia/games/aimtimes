﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameDirector : MonoBehaviour
{
    // CanvasMain
    public GameObject textStatus;

    // CanvasCountdown
    public GameObject canvasCountdown;
    public GameObject imageCountdown;
    public GameObject textCountdownSeconds;

    // CanvasGame
    public GameObject canvasGame;
    public GameObject textTimeLimitSeconds;
    public GameObject textScore;
    public GameObject textProbrem;
    public GameObject textAnswer;
    public GameObject button1;
    public GameObject button2;
    public GameObject button3;
    public GameObject button4;
    public GameObject button5;
    public GameObject button6;
    public GameObject button7;
    public GameObject button8;
    public GameObject button9;
    public GameObject button0;
    public GameObject buttonC;
    public GameObject buttonE;

    /// <summary>
    /// Score display
    /// </summary>
    public static int scoreDisplay;

    /// <summary>
    /// Passed seconds
    /// </summary>
    private float passedSeconds;

    /// <summary>
    /// Seconds of keep status text display
    /// </summary>
    private float statusTextDisplaySeconds;

    /// <summary>
    /// Countdown seconds (initial value)
    /// </summary>
    private float countdownSeconds;

    /// <summary>
    /// Countdown seconds display
    /// </summary>
    private float countdownSecondsDisplay;

    /// <summary>
    /// Time limit seconds (initial value)
    /// </summary>
    private float timeLimitSeconds;

    /// <summary>
    /// Time limit seconds display
    /// </summary>
    private float timeLimitSecondsDisplay;

    /// <summary>
    /// Probrem display
    /// </summary>
    private string probremDisplay;

    /// <summary>
    /// Answer display (Inputted answer)
    /// </summary>
    private string answerDisplay;

    /// <summary>
    /// Expected answer
    /// </summary>
    private int expectedAnswer;

    /// <summary>
    /// Buttons coordinates
    /// </summary>
    private readonly Vector2Int[] coordinates = new Vector2Int[]
    {
        new Vector2Int(-270, 1050), // 1
        new Vector2Int(0,    1050), // 2
        new Vector2Int(270,  1050), // 3
        new Vector2Int(-270, 780),  // 4
        new Vector2Int(0,    780),  // 5
        new Vector2Int(270,  780),  // 6
        new Vector2Int(-270, 510),  // 7
        new Vector2Int(0,    510),  // 8
        new Vector2Int(270,  510),  // 9
        new Vector2Int(-270, 240),  // 0
        new Vector2Int(0,    240),  // C
        new Vector2Int(270,  240),  // =
    };

    // Start is called before the first frame update
    void Start()
    {
        // Initialize
        scoreDisplay = 0;

        this.passedSeconds = 0.0f;
        this.statusTextDisplaySeconds = 1.0f;
        this.countdownSeconds = 4.0f; // 4 seconds = [3, 2, 1, START]
        this.timeLimitSeconds = 20.0f + this.countdownSeconds;

        this.countdownSecondsDisplay = this.countdownSeconds;
        this.timeLimitSecondsDisplay = this.timeLimitSeconds;
        this.probremDisplay = "";
        this.answerDisplay = "";

        // Show countdown canvas
        this.canvasCountdown.SetActive(true);

        // Hide game canvas
        this.canvasGame.SetActive(false);

        // Hide status text
        this.textStatus.SetActive(false);

        // Create new probrem
        this.CreateNewProbrem();
    }

    // Update is called once per frame
    void Update()
    {
        // Update seconds
        this.passedSeconds += Time.deltaTime;
        this.countdownSecondsDisplay -= Time.deltaTime;
        this.timeLimitSecondsDisplay -= Time.deltaTime;

        // Update text
        this.textScore.GetComponent<Text>().text = scoreDisplay.ToString();
        this.textProbrem.GetComponent<Text>().text = this.probremDisplay;
        this.textAnswer.GetComponent<Text>().text = this.answerDisplay;
        this.textCountdownSeconds.GetComponent<Text>().text = Mathf.Floor(this.countdownSecondsDisplay).ToString();
        this.textTimeLimitSeconds.GetComponent<Text>().text = this.timeLimitSecondsDisplay.ToString("F2");

        // Update image
        this.imageCountdown.GetComponent<Image>().fillAmount = this.countdownSecondsDisplay % 1.0f;

        // When START!!
        if (this.passedSeconds > (this.countdownSeconds - this.statusTextDisplaySeconds))
        {
            // Hide countdown canvas
            this.canvasCountdown.SetActive(false);

            // Show "START!!"
            this.textStatus.SetActive(true);
            this.textStatus.GetComponent<Text>().text = "START!!";
        }
 
        // While gaming...
        if (this.passedSeconds > this.countdownSeconds)
        {
            // Hide "START!!"
            this.textStatus.SetActive(false);

            // Show game canvas
            this.canvasGame.SetActive(true);
        }
        
        // When FINISH!!
        if (this.passedSeconds > this.timeLimitSeconds)
        {
            // Hide game canvas
            this.canvasGame.SetActive(false);

            // Show "FINISH!!"
            this.textStatus.SetActive(true);
            this.textStatus.GetComponent<Text>().text = "FINISH!!";
        }
        
        // Load ResultScene
        if (this.passedSeconds > (this.timeLimitSeconds + this.statusTextDisplaySeconds))
        {
            SceneManager.LoadScene("TitleScene");
        }
    }

    /// <summary>
    /// Create new probrem
    /// </summary>
    private void CreateNewProbrem()
    {
        // Create new probrem text
        int a = UnityEngine.Random.Range(1, 10);
        int b = UnityEngine.Random.Range(1, 10);
        string probrem = $"{a} × {b} = ";

        // Set probrem display
        this.probremDisplay = probrem;

        // Set expected answer
        this.expectedAnswer = a * b;
    }

    /// <summary>
    /// Reset answer display
    /// </summary>
    private void ResetAnswerDisplay()
    {
        this.answerDisplay = "";
    }

    /// <summary>
    /// Shuffle buttons coordinates
    /// </summary>
    private void ShuffleButtons()
    {
        // Make new coordinates order
        var newCoordinates = this.coordinates.OrderBy(i => Guid.NewGuid()).ToArray();

        // Transform buttons position
        GameObject[] buttons =
        {
            this.button1,
            this.button2,
            this.button3,
            this.button4,
            this.button5,
            this.button6,
            this.button7,
            this.button8,
            this.button9,
            this.button0,
            this.buttonC,
            this.buttonE,
        };

        for (int i = 0; i < newCoordinates.Count(); i++)
        {
            var transform = buttons[i].GetComponent<RectTransform>();
            transform.anchoredPosition = newCoordinates[i];
        }
    }

    /// <summary>
    /// Set answer display when number button down
    /// </summary>
    private void ButttonNumberDown(string number)
    {
        // Add button number to answer display when less than 2 digits
        if (this.answerDisplay.Length < 2)
        {
            this.answerDisplay += number;
        }
    }

    public void Button1Down()
    {
        this.ButttonNumberDown("1");
    }

    public void Button2Down()
    {
        this.ButttonNumberDown("2");
    }

    public void Button3Down()
    {
        this.ButttonNumberDown("3");
    }

    public void Button4Down()
    {
        this.ButttonNumberDown("4");
    }

    public void Button5Down()
    {
        this.ButttonNumberDown("5");
    }

    public void Button6Down()
    {
        this.ButttonNumberDown("6");
    }

    public void Button7Down()
    {
        this.ButttonNumberDown("7");
    }

    public void Button8Down()
    {
        this.ButttonNumberDown("8");
    }

    public void Button9Down()
    {
        this.ButttonNumberDown("9");
    }

    public void Button0Down()
    {
        this.ButttonNumberDown("0");
    }

    public void ButtonCDown()
    {
        this.ResetAnswerDisplay();
    }

    public void ButtonEDown()
    {
        if (this.answerDisplay == this.expectedAnswer.ToString())
        {
            // Correct: increase score by number of answer
            scoreDisplay += this.expectedAnswer;
        }
        else
        {
            // Wrong: decrease score by number of answer
            scoreDisplay -= this.expectedAnswer;
        }

        // Shuffle buttons
        this.ShuffleButtons();

        // Reset answer display
        this.ResetAnswerDisplay();

        // Create new probrem
        this.CreateNewProbrem();
    }

    public void ButtonRetryDown()
    {
        SceneManager.LoadScene("GameScene");
    }
}
