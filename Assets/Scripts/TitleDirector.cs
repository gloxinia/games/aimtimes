﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleDirector : MonoBehaviour
{
    public GameObject textScore;
    public GameObject textHighScore;
    public GameObject textVersion;

    /// <summary>
    /// High score display
    /// </summary>
    public static int highScoreDisplay;

    // Start is called before the first frame update
    void Start()
    {
        // Set high score
        var score = GameDirector.scoreDisplay;

        if (score > highScoreDisplay)
        {
            highScoreDisplay = score;
        }

        // Show score and high score
        this.textScore.GetComponent<Text>().text = score.ToString();
        this.textHighScore.GetComponent<Text>().text = highScoreDisplay.ToString();

        // Show version
        this.textVersion.GetComponent<Text>().text = "Version " + Application.version;
    }

    /// <summary>
    /// When button "START" down
    /// </summary>
    public void ButtonStartDown()
    {
        SceneManager.LoadScene("GameScene");
    }

    /// <summary>
    /// When button "TWEET" down
    /// </summary>
    public void ButtonTweetDown()
    {
        // Make text
        string text;
        
        if (highScoreDisplay > 0)
        {
            text = $"SCORE: {highScoreDisplay}";
        } else
        {
            text = "Play AIMTIMES!!";
        }

        text = UnityWebRequest.EscapeURL(text);

        // URL, Hashtags
        string url = "https://gloxinia.gitlab.io/games/aimtimes/";
        string hashtags = "GLOXINIA_AIMTIMES";

        // Tweet
        Application.OpenURL($"https://twitter.com/intent/tweet?text={text}&url={url}&hashtags={hashtags}");
    }
}
